package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.PageSelectedProductDto
import dv.spring.kotlin.service.SelectedProductService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class SelectedProductController {
    @Autowired
    lateinit var selectedProductService: SelectedProductService
    @GetMapping("/selectedProduct/productName")
    fun getSelectedProductWithProductName(@RequestParam("name") name: String,
                                          @RequestParam("page") page:Int,
                                          @RequestParam("pageSize") pageSize:Int): ResponseEntity<Any> {
        val output = selectedProductService.getSelectedProductWithProductName(name,page,pageSize)
        return ResponseEntity.ok(PageSelectedProductDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                selectedProducts = MapperUtil.INSTANCE.mapSelectedProductDto(output.content)))
    }
}