package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.PageShoppingCartDto
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService
    @GetMapping("/shoppingCart")
    fun  getShoppingCart(): ResponseEntity<Any>{
        val shoppingCarts = shoppingCartService.getShoppingCart()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCarts))
    }
    @GetMapping("/allshoppingCart")
    fun getShoppingCartWithPage(@RequestParam("page") page:Int,
                               @RequestParam("pageSize") pageSize:Int): ResponseEntity<Any>{
        val output = shoppingCartService.getShoppingCartWithPage(page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCartCustomerDto(output.content)))
    }
    @GetMapping("/shoppingCart/productName")
    fun getShoppingCartWithProductName(@RequestParam("name") name: String,
            @RequestParam("page") page:Int,
                                @RequestParam("pageSize") pageSize:Int): ResponseEntity<Any>{
        val output = shoppingCartService.getShoppingCartWithProductName(name,page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCartCustomerDto(output.content)))
    }

}