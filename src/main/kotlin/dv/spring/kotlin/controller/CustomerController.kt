package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService
    @GetMapping("/customer")
    fun getCustomer(): ResponseEntity<Any>{
        val customers = customerService.getCustomer()
        return  ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomer(customers))
    }
    @GetMapping("/customer/query")
    fun getProduct(@RequestParam("name") name:String): ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomer(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customers/partialQuery")
    fun getCustomersPartial(@RequestParam("name")name: String,
                            @RequestParam(value = "email", required = false) email:String?): ResponseEntity<Any>{
        val output: List<CustomerDto> = MapperUtil.INSTANCE.mapCustomer(
                customerService.getCustomerByPartialNameOrEmail(name,name)
        )
        return ResponseEntity.ok(output)
    }
    @GetMapping("/customer/{address}")
    fun getWhoseAddress(@PathVariable("address") name: String): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomer(
                customerService.getWhoseByAddress(name)
        )
        return ResponseEntity.ok(output)
    }
    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto): ResponseEntity<Any>{
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomer(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @PostMapping("/customer/address/{addrId}")
    fun addCustomerWithExistAddress(@RequestBody customerDto: CustomerDto,
                                    @PathVariable addrId:Long): ResponseEntity<Any>{
        val output = customerService.save(addrId,MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomer(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customer/status")
    fun getCustomerFromStatus(@RequestParam(name="status") status: String): ResponseEntity<Any>{
        return ResponseEntity.ok(customerService.fingByStatus(status))
    }
    @GetMapping("/customer/product")
    fun getCustomerFromBoughtProduct(@RequestParam(name="name") name: String) : ResponseEntity<Any>{
        return ResponseEntity.ok(customerService.findByBoughtProduct(name))
    }
    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id") id:Long): ResponseEntity<Any>{
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomer(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the customer id is not found")
    }
}