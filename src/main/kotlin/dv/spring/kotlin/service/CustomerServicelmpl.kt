package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServicelmpl: CustomerService {
    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao
    override fun findByBoughtProduct(name: String): List<Customer> {
        return shoppingCartDao.findByProductName(name)
                .map { shoppingCart -> shoppingCart.customer}
                .toSet().toList()
    }

    override fun fingByStatus(status: String): List<Customer> {
        return customerDao.findByStatus(status)
    }

    @Autowired
    lateinit var addressDao: AddressDao
    @Autowired
    lateinit var customerDao: CustomerDao

    @Transactional
    override fun save(addrId: Long, customer: Customer): Customer {
        val address = addressDao.findById(addrId)
        val customer = customerDao.save(customer)
        customer.defaultAddress = address
        address?.customers?.add(customer)
        return customer
    }


    @Transactional
    override fun save(customer: Customer): Customer {
        val address = customer.defaultAddress?.let { addressDao.save(it) }
        val customer = customerDao.save(customer)
        address?.customers?.add(customer)
        return customer
    }

    override fun getWhoseByAddress(name: String): List<Customer> {
        return customerDao.getWhoseByAddress(name)
    }

    override fun getCustomerByPartialNameOrEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByNameOrEmail(name,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    override fun getCustomerByName(name: String): Customer?
    = customerDao.getCustomerByName(name)



    override fun getCustomer(): List<Customer> {
        return customerDao.getCustomer()
    }

}