package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer

interface CustomerService {
    fun getCustomer(): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameOrEmail(name: String, email: String): List<Customer>
    fun getWhoseByAddress(name: String): List<Customer>
    fun save(customer: Customer): Customer
    fun save(addrId: Long, customer: Customer): Customer
    fun fingByStatus(status: String): List<Customer>
    fun findByBoughtProduct(name: String): List<Customer>
    fun remove(id: Long): Customer?
}