package dv.spring.kotlin.service

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCart(): List<ShoppingCart>
    fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartWithProductName(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
}