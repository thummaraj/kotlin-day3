package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCartStatus

data class ShoppingCartDto(var status:ShoppingCartStatus? = ShoppingCartStatus.WAIT,
                           var selectedProducts:List<SelectedProductDto>? = mutableListOf<SelectedProductDto>(),
                           var customer: CustomerDto?= null
)