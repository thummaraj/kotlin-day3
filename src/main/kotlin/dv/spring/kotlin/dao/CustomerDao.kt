package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer

interface CustomerDao {
    fun getCustomer(): List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByNameOrEmail(name: String, email: String): List<Customer>
    fun getWhoseByAddress(name: String): List<Customer>
    fun save(customer: Customer): Customer
    fun findByStatus(status: String): List<Customer>
    fun findById(id: Long): Customer?
}